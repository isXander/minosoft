import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    val kotlinVersion: String by System.getProperties()
    kotlin("jvm") version kotlinVersion
    application
}

data class Natives(val lwjgl: String, val javaFX: String, val zstd: String)
enum class Arch {
    X86, X64, ARM, ARM64
}
enum class OperatingSystem {
    WINDOWS, LINUX, MACOS
}
val natives = Pair(
    System.getProperty("os.name")!!,
    System.getProperty("os.arch")!!
).let { (name, arch) ->

    data class OsInfo(val os: OperatingSystem, val arch: Arch)

    val osInfo = when {
        arrayOf("Linux", "FreeBSD", "SunOS", "Unit").any { name.startsWith(it) } ->
            if (arrayOf("arm", "aarch64").any { arch.startsWith(it) })
                OsInfo(OperatingSystem.LINUX, if (arch.contains("64") || arch.startsWith("armv8")) Arch.ARM64 else Arch.ARM)
            else
                OsInfo(OperatingSystem.LINUX, Arch.X64)
        arrayOf("Mac OS X", "Darwin").any { name.startsWith(it) }                ->
            OsInfo(OperatingSystem.MACOS, if (arch.startsWith("aarch64")) Arch.ARM64 else Arch.X64)
        arrayOf("Windows").any { name.startsWith(it) }                           ->
            if (arch.contains("64"))
                OsInfo(OperatingSystem.WINDOWS, if (arch.startsWith("aarch64")) Arch.ARM64 else Arch.X64)
            else
                OsInfo(OperatingSystem.WINDOWS, Arch.X86)
        else -> throw Error("Unrecognized or unsupported platform. Please set \"lwjgl\" manually")
    }

    when (osInfo.os) {
        OperatingSystem.LINUX -> when (osInfo.arch) {
            Arch.ARM64 -> Natives(lwjgl = "linux-arm64", javaFX = "linux-aarch64", zstd = "linux_aarch64")
            Arch.ARM -> Natives(lwjgl = "linux-arm32", javaFX = "linux.aarch32", zstd = "linux_aarch32")
            Arch.X64 -> Natives(lwjgl = "linux", javaFX = "linux", zstd = "linux_amd64")
            else -> error("Unrecognized or unsupported architecture")
        }
        OperatingSystem.MACOS -> when (osInfo.arch) {
            Arch.ARM64 -> Natives(lwjgl = "macos-arm64", javaFX = "mac-aarch64", zstd = "darwin_aarch64")
            Arch.X64 -> Natives(lwjgl = "macos", javaFX = "mac", zstd = "darwin_x86_64")
            else -> error("Unrecognized or unsupported architecture")
        }
        OperatingSystem.WINDOWS -> when (osInfo.arch) {
            Arch.X86 -> Natives(lwjgl = "windows-x86", javaFX = "win-x86", zstd = "win_x86")
            Arch.X64 -> Natives(lwjgl = "windows", javaFX = "win", zstd = "win_amd64")
            else -> error("Unrecognized or unsupported architecture")
        }
    }
}

application {
    mainClass.set("de.bixilon.minosoft.Minosoft")
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation(platform("org.lwjgl:lwjgl-bom:${property("lwjglVersion")}"))
    implementation("org.lwjgl", "lwjgl")
    implementation("org.lwjgl", "lwjgl-glfw")
    implementation("org.lwjgl", "lwjgl-openal")
    implementation("org.lwjgl", "lwjgl-opengl")
    implementation("org.lwjgl", "lwjgl-stb")
    runtimeOnly("org.lwjgl", "lwjgl", classifier = "natives-${natives.lwjgl}")
    runtimeOnly("org.lwjgl", "lwjgl-glfw", classifier = "natives-${natives.lwjgl}")
    runtimeOnly("org.lwjgl", "lwjgl-openal", classifier = "natives-${natives.lwjgl}")
    runtimeOnly("org.lwjgl", "lwjgl-opengl", classifier = "natives-${natives.lwjgl}")
    runtimeOnly("org.lwjgl", "lwjgl-stb", classifier = "natives-${natives.lwjgl}")

    implementation("org.slf4j:slf4j-api:1.7.36")
    implementation("com.google.code.gson:gson:2.9.0")
    implementation("com.google.guava:guava:31.1-jre")
    implementation("dnsjava:dnsjava:3.5.0")
    implementation("org.xeustechnologies:jcl-core:2.8")
    implementation("net.sourceforge.argparse4j:argparse4j:0.9.0")
    implementation("org.jline:jline:3.21.0")
    implementation("com.github.bixilon:ascii-table:5375a4f")
    implementation("org.l33tlabs.twl:pngdecoder:1.0")
    implementation("de.bixilon:kotlin-glm:0.9.9.1-6")
    implementation("com.github.oshi:oshi-core:6.1.6")
    implementation("com.github.luben:zstd-jni:1.5.2-2:${natives.zstd}")
    implementation("de.bixilon.bixilon:mbf-kotlin:1.0") {
        exclude(group = "com.github.luben", module = "zstd-jni")
    }
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.2")
    implementation("org.kamranzafar:jtar:2.3")
    implementation("org.reflections:reflections:0.10.2")
    implementation("de.bixilon:kutil:1.10.7")
    implementation("it.unimi.dsi:fastutil-core:8.5.8")

    val nettyVersion: String by project
    implementation("io.netty:netty-buffer:$nettyVersion")
    implementation("io.netty:netty-transport-native-epoll:$nettyVersion")
    implementation("io.netty:netty-handler:$nettyVersion")

    val ikonliVersion: String by project
    implementation("org.kordamp.ikonli:ikonli-fontawesome5-pack:$ikonliVersion")
    implementation("org.kordamp.ikonli:ikonli-javafx:$ikonliVersion")

    val javaFXVersion: String by project
    implementation("org.openjfx:javafx-base:$javaFXVersion:${natives.javaFX}")
    implementation("org.openjfx:javafx-graphics:$javaFXVersion:${natives.javaFX}")
    implementation("org.openjfx:javafx-controls:$javaFXVersion:${natives.javaFX}")
    implementation("org.openjfx:javafx-fxml:$javaFXVersion:${natives.javaFX}")
    implementation("de.bixilon.javafx:javafx-svg:0.2")

    testImplementation(kotlin("test"))
}

tasks {
    withType<JavaCompile> {
        sourceCompatibility = "11"
        targetCompatibility = "11"
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }
}
